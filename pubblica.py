from time import sleep
from watermark import *
import facebook
import requests
import json
from random import randint
import re
import sqlite3
from PIL import Image, ImageDraw, ImageFont, ImageEnhance
import os, sys
    
cfg = {
      "page_id"      : "ID_della_pagina",
      "access_token" : "token_della_pagina_di_solito_164_caratteri"
      }
    
graph = facebook.GraphAPI(cfg['access_token'])
resp = graph.get_object(cfg["page_id"], fields='access_token')
page_access_token = resp['access_token']
graph = facebook.GraphAPI(page_access_token)

def pubblica_testo(title):
    """ 
    # puoi sperimentare con attachment, se vuoi allegare link o minchiate, 
    # togli i commenti e a .put_wall_post fagli mandare anche attachment=attachment
    attachment = {
      'name': '',
      'link': url,
      'caption': '',
      'description': title + ";",
      'picture': url
    }
    """
    graph.put_wall_post(message=title) # , attachment=attachment) puoi sperimentare
    print "PUBBLICATO TESTO!"

def pubblica_foto(url, title):
    filename = download(url, title)
    if 'gif' not in filename:
        print "Adding Watermark"
        add_watermark(filename)
    graph.put_photo(image=open(filename, 'rb'), message=title)
    print "*** PUBBLICATA FOTO e TESTO!!"

if __name__ == '__main__':
    # per pubblicare foto e testo
    post(url_della_foto, "testo ca ti piaci o titolo immagine")
    # per pubblicare solo testo
    pubblica_testo("U testo ca ti piaci")
